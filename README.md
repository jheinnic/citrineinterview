JSR 363 Units of Measurement Demo

### Build
mvn install

### Run (Presumes current release of 0.0.8)
java -jar units-webapp/target/units-webapp-0.0.8.jar

### Explore
- Open a browser
- Navigate to http://localhost:8090

This will provide you with a Swagger documentation page for an API with two RESTful endpoints.

# http://localhost:8090/units/si?units=<Expression>

Given an unit expression made of *, /, (), and any supported unit names or labels, get back a JSON
document with an equivalent reduction to base SI units (s, m, kg, K, mol, A, and cd) and that minimal 
formula's equivalent unit ratio.  Spaces may not be used between either units or operators in this
present release, but that restriction may easily be lifted in the future.

Note that you can use multiples of units, but at this release it is necessary to explicitly multiply or
divide to do so.  For example, "20*seconds" will be accepted, but "20 seconds" and "20seconds" will both
yield an error.

Examples:

- lb/L
http://localhost:8090/units/si?units=lb%2FL
{
  "multiplication_factor": 453.59237,
  "unit_name": "kg/m³"
}

- (5000*ounce)/minute
{
  "multiplication_factor": 2.3624602604166665,
  "unit_name": "kg/s"
}

- pound*kL/(hectare*mgram)/minute
http://localhost:8090/units/si?units=pound*kL%2F(hectare*mgram)/minute
{
  "multiplication_factor": 0.7559872833333333,
  "unit_name": "m/s"
}

- (kg*day/(ounce*Mm))*(cm/(dram*second)/s)
http://localhost:8090/units/si?units=(kg*day%2F(ounce*Mm))*(cm%2F(dram*second)%2Fs)
{
  "multiplication_factor": 17.200545061761055,
  "unit_name": "1/(s·kg)"
}

# http://localhost:8090/units/si/convert

This variant is a POST operation that provides a numeric value and a unit expression.  As with the
first API method, the unit expression is reduced to base SI units.  The resulting unit expression and
conversion factor are both returned this time as well.  Additionally , the input value is multiplied by 
the conversion ratio and its equivalent value using the reduced units is reported.

Example:

curl -X POST --header 'Content-Type: application/json;charset=UTF-8;schema=citrineUnits.json;message=QuantityToConvert;revision=1.0.0' --header 'Accept: application/json' -d '{
  "input": 90,
  "units": "l/cm"
}' 'http://localhost:8090/units/si/convert'

{
  "multiplication_factor": 0.1,
  "output": 9,
  "unit_name": "m²"
}

### Inside

The bulk of interesting stuff is found in the units-services project  JSR-363 includes unit parsing, 
formatting, and conversion for a much broader array of SI units than was implemented here.  The purpose
was to provide a small number of well-known conversions to demonstrate how the library may be used to 
create customized units of measure.

In an unfortunate naming twist, the CitrineUnitFormat class and its associated message.properties file carry
most of the responsibility for parsing unit expressions.

The CitrineUnits class carries much of the responsiblity for defining type conversion semantics

The NamesAndSymbols class is provides the mapping from unit to name that is used when formatting unit
expressions

The code that performs tasks with these functional units is found in CitrineUnitFormatService and
CitruneSystemOfUnitsService.

See http://download.oracle.com/otn-pub/jcp/units-1_0-final-eval-spec/JSR363Specification_Final_Eval.pdf?AuthParam=1511294143_1e12c4c28b7b763b74d720f33e2bc801 for 
more information in the reference documentation.