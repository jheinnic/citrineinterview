swagger: '2.0'
info:
  description: Units Conversion Interview Challenge
  version: 1.0.0
  title: Citrine Interview
  license:
    name: MIT
host: citrine.jchein.ngrok.info
basePath: /units
tags:
  - name: unit_conversion
    description: On conversion from arbitrary to base SI units
schemes:
  - http
consumes:
  - application/json
produces:
  - application/json
parameters:
  units:
    name: units
    in: query
    description: >-
      Unit string consisting of any number of supported unit names or  symbols
      multiplied or divided any number of times.
    required: true
    type: string
  convert_quantity_body:
    in: body
    name: body
    required: true
    schema:
      $ref: '#/definitions/quantity_to_convert'
paths:
  /si:
    get:
      tags: [
        unit_conversion
      ]
      summary: Convert Units to Base SI
      description: >-
        Given units formula consisting of any combination of supported units,
        return a compatible reduced units formula composed only of base SI
        types, and a conversion ratio from converting quantity values for the
        input units to values for the output units.
      parameters:
        - $ref: '#/parameters/units'
      responses:
        200:
          description: >-
            Result code and payload returned when unit conversion is
            successful.  JSON message payload reports the converted Base SI
            units and conversion ratio.
          schema:
            $ref: '#/definitions/converted_units'
  /si/convert:
    post:
      tags: [
        unit_conversion
      ]
      summary: Convert Quantity and Units to Base SI
      description: >-
        Given an arbitrary quantity and it units, which must be composed from a
        known platforms native units, find the compatible units formula
        consisting only of base SI units, convert the input quantitys value to
        the base SI-based unit.  Return result of conversion, target SI  units
        for conversion, and actual conversion ratio used.
      parameters:
        - $ref: '#/parameters/convert_quantity_body'
      responses:
        200:
          description: >-
            JSON Message providing the computation results.  Includes unit
            formula in base SI units and numerical result of conversion from
            input to output units, and ratio value used to compute output 
            value.
          schema:
            $ref: '#/definitions/converted_quantity'
definitions:
  converted_quantity:
    type: object
    required:
      - multiplication_factor
      - output
      - unit_name
    properties:
      multiplication_factor:
        type: number
        format: double
        minimum: 0
        exclusiveMinimum: true
        description: >-
          Conversion ratio to returned base SI unit from query quantity used 
          to retrieve it.
      output:
        type: number
        format: double
        description: Output value after converting query quantity to base SI units.
      unit_name:
        type: string
        description: Unit of equivalent dimension, expressed as a formula for base SI units
    description: >-
      Output from a quantity conversion request, consisting of a double
      precision floating point value, a compound unit expression made of base SI
      units, and a conversion ratio from input to output quantities resulting
      from the change of units.
  converted_units:
    type: object
    required:
      - multiplication_factor
      - unit_name
    properties:
      multiplication_factor:
        type: number
        format: double
        minimum: 0
        exclusiveMinimum: true
        description: >-
          Conversion ratio to returned base SI unit from the query unit used 
          to retrieve it.
      unit_name:
        type: string
        minLength: 1
        description: Unit of equivalent dimension, expressed as a formula for base SI units
    description: >-
      Output from a units conversion request, consisting of a compound unit
      expression made of base SI units and the conversion ratio from for
      hypothetical quantities when mapped from given input units to this
      messages output units.
  error_result:
    type: object
    required:
      - message
    properties:
      code:
        type: string
        description: >-
          A value from an application enumeration associated with a known 
          error condition, if the nature of the error is well known.
      message:
        type: string
        minLength: 3
        description: A human readable message describing the error
      trace:
        type: array
        description: An optional stack trace, if available
        items:
          type: string
  quantity_to_convert:
    type: object
    required:
      - input
      - units
    properties:
      input:
        type: number
        format: double
        description: >-
          Double precision floating point value defining the quantity to be
          converted to system units.
      units:
        type: string
        minLength: 1
        pattern: /[A-Za-z\/*()]+/
        description: >-
          Formula expressing units for the the given input quantity to be
          converted from.
    description: >-
      Input message requesting conversion of an input quantity, given its 
      value and units, to an equivalent value in compatible Base SI units.

