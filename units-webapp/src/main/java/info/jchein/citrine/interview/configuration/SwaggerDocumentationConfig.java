package info.jchein.citrine.interview.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


@Configuration
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen",
	date = "2016-08-17T12:53:25.823Z")
public class SwaggerDocumentationConfig
{

	ApiInfo apiInfo()
	{
		return new ApiInfoBuilder().title("Citrine Interview")
			.description("Units Conversion Interview Challenge")
			.license("MIT")
			.licenseUrl("")
			.termsOfServiceUrl("")
			.version("1.0.0")
			.contact(new Contact("John Heinnickel", "http://www.jchein.info/", "jheinnic@gmail.com"))
			.build();
	}


	@Bean
	public Docket customImplementation()
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
			.apis(RequestHandlerSelectors.basePackage("info.jchein.citrine.interview.controller"))
			.paths(PathSelectors.ant("/units/si/**"))
			.build()
			// .directModelSubstitute(org.joda.time.LocalDate.class, java.sql.Date.class)
			// .directModelSubstitute(org.joda.time.DateTime.class, java.util.Date.class)
			.apiInfo(apiInfo());
	}

}
