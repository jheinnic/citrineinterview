package info.jchein.citrine.interview.conversion;

import javax.measure.Unit;
import javax.measure.format.UnitFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class UnitConverter
implements Converter<String, Unit<?>>
{
	private static final Logger LOG = LoggerFactory.getLogger(UnitConverter.class);

	@Autowired
	private UnitFormat unitFormat;

	@Override
	public Unit<?> convert(String source)
	{
		try {
			return unitFormat.parse(source);
		}
		catch (Exception exp) {
			LOG.error("Could not interpret input type formula, {}", source);
			throw new IllegalArgumentException("Could not interpret input type formula", exp);
		}
	}


	public void setUnitFormat(final UnitFormat unitFormat)
	{
		this.unitFormat = unitFormat;
	}

}
