package info.jchein.citrine.interview.controller;


import javax.measure.IncommensurableException;
import javax.measure.UnconvertibleException;
import javax.measure.Unit;
import javax.measure.UnitConverter;
import javax.measure.format.UnitFormat;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.jchein.citrine.interview.api.ApiMessageTypes;
import info.jchein.citrine.interview.api.SiApi;
import info.jchein.citrine.interview.model.ConvertedQuantity;
import info.jchein.citrine.interview.model.ConvertedUnit;
import info.jchein.citrine.interview.model.QuantityToConvert;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-17T14:03:32.044-07:00")

@RestController
public class SiApiController implements SiApi {
	private static Logger LOGGER = LoggerFactory.getLogger(SiApiController.class);

	@Autowired
	@Qualifier("Citrine")
	UnitFormat unitFormat;

	@Override
	public ResponseEntity<ConvertedQuantity>
	siConvertPost(@RequestBody @NotNull @Valid QuantityToConvert body)
	{
		final ConvertedQuantity retVal = new ConvertedQuantity();
		final Unit<?> parsedUnit = body.getUnits();
		final Unit<?> systemUnit;
		try {
			systemUnit = parsedUnit.getSystemUnit();
			retVal.setUnitName(unitFormat.format(systemUnit));
		}
		catch (Exception exp) {
			LOGGER.error("Could not interpret input type formula, {}", parsedUnit);
			return ResponseEntity.badRequest()
				.<ConvertedQuantity> body(null);
		}

		// This is JSR311 unit converter, not Spring deserializer by same class name
		final UnitConverter unitConvert;
		try {
			unitConvert = parsedUnit.getConverterToAny(systemUnit);
			if (unitConvert.isLinear()) {
				retVal.setMultiplicationFactor(unitConvert.convert(1));
				retVal.setOutput(
					unitConvert.convert(
						body.getInput()
						.doubleValue()));
			} else {
				LOGGER.error(
					"No linear conversion from {} to {}", parsedUnit, retVal.getUnitName());
				// throw new UnsupportedOperationException("Non-linear conversion");
				return ResponseEntity.unprocessableEntity()
					.<ConvertedQuantity> body(retVal);
			}
		}
		catch (UnconvertibleException | IncommensurableException e) {
			LOGGER.error("{} cannot be converted to system units", parsedUnit);
			// throw new UnsupportedOperationException("No conversion found");
			return ResponseEntity.unprocessableEntity()
				.<ConvertedQuantity> body(retVal);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(ApiMessageTypes.CONVERTED_QUANTITY.getMediaType());
		return new ResponseEntity<>(retVal, headers, HttpStatus.OK);
	}


	@Override
	public ResponseEntity<ConvertedUnit> siGet(
		@NotNull @RequestParam(value = "units", required = true) Unit<?> parsedUnit
	) {
		final ConvertedUnit retVal = new ConvertedUnit();
		final Unit<?> systemUnit;
		try {
			systemUnit = parsedUnit.getSystemUnit();
			retVal.setUnitName(unitFormat.format(systemUnit));
		}
		catch (Exception exp) {
			LOGGER.error("Could not interpret input type formula, {}", parsedUnit);
			return ResponseEntity.badRequest()
				.<ConvertedUnit> body(null);
		}

		final UnitConverter unitConvert;
		try {
			unitConvert = parsedUnit.getConverterToAny(systemUnit);
			if (unitConvert.isLinear()) {
				retVal.setMultiplicationFactor(unitConvert.convert(1));
			} else {
				LOGGER
.error(
					"Non-linear conversion from {} to {} has no ratio", parsedUnit, retVal.getUnitName());
				// throw new UnsupportedOperationException("Non-linear conversion");
				return ResponseEntity.unprocessableEntity()
					.<ConvertedUnit> body(retVal);
			}
		}
		catch (UnconvertibleException | IncommensurableException e) {
			LOGGER.error("No conversion found from {} to {}", parsedUnit, retVal.getUnitName());
			// throw new UnsupportedOperationException("No conversion found");
			return ResponseEntity.unprocessableEntity()
				.<ConvertedUnit> body(retVal);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(ApiMessageTypes.CONVERTED_UNIT.getMediaType());
		return new ResponseEntity<>(retVal, headers, HttpStatus.OK);
    }
}
