package info.jchein.citrine.interview.controller;


import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Home redirection to swagger api documentation
 */
@Controller
public class HomeController
{
	@RequestMapping(value = "/")
	public void index(HttpServletResponse response)
	{
		try {
			response.sendRedirect("swagger-ui.html");
		}
		catch (IOException e) {
			response.setStatus(500);
			throw new RuntimeException(e);
		}
	}
}
