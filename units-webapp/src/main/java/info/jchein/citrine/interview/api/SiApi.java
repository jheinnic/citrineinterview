package info.jchein.citrine.interview.api;


import javax.measure.Unit;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.jchein.citrine.interview.model.ConvertedQuantity;
import info.jchein.citrine.interview.model.ConvertedUnit;
import info.jchein.citrine.interview.model.QuantityToConvert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-17T14:03:32.044-07:00")

@RestController
@Api(value = "SI Unit Conversion")
public interface SiApi {

    @ApiOperation(value = "Convert Quantity and Units to Base SI",
		notes = "Given an arbitrary quantity and it units, which must be composed from a known platforms native units, find the compatible units formula consisting only of base SI units and convert the input quantity's value to base SI-based unit.  Return the converted quantity, conversion ratio, and output SI-based unit formula.",
		response = ConvertedQuantity.class, tags = { "SI Unit Conversion" })
    @ApiResponses(value = { 
		@ApiResponse(code = 200,
			message = "Message payload contains computer unit conversion result.  Output unit formula is expressed as operations on minimal required number of base SI units and numerical result of conversion from input to output units, and ratio value used to compute output  value.",
			response = ConvertedQuantity.class)
	})
	@RequestMapping(value = "/units/si/convert",
        produces = { ApiMessageTypes.CONVERTED_QUANTITY_VALUE },
        consumes = { ApiMessageTypes.QUANTITY_TO_CONVERT_VALUE },
        method = RequestMethod.POST)
    ResponseEntity<ConvertedQuantity> siConvertPost(
   	 @ApiParam(
   		 value = "JSON body content encapsulating a floating point quantity and its associated units formula, which need not be composed of only base SI unit terms.",
   		 required = true)
   	 @RequestBody
   	 QuantityToConvert body
   );


   @ApiOperation(value = "Convert Units to Base SI",
   	notes = "Given units formula consisting of any combination of supported units, return a compatible reduced units formula composed only of base SI types, and a conversion ratio from converting quantity values for the input units to values for the output units.", response = ConvertedUnit.class, tags={ "SI Unit Conversion", })
   @ApiResponses(value = { 
		@ApiResponse(code = 200,
			message = "Result code and payload returned when unit conversion is successful.  JSON message payload reports the converted Base SI units and conversion ratio.",
			response = ConvertedUnit.class)
	})
	@RequestMapping(value = "/units/si",
      produces = { ApiMessageTypes.CONVERTED_UNIT_VALUE },
      method = RequestMethod.GET)
   ResponseEntity<ConvertedUnit> siGet(
   	@ApiParam(
   		value = "Unit string consisting of any number of supported unit names or symbols multiplied or divided together any number of times.",
   		required = true)
   	@RequestParam(value = "units", required = true)
   	Unit<?> parsedUnit
   );
}
