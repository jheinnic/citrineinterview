package info.jchein.citrine.interview.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen",
	date = "2016-08-17T13:59:12.955-07:00")
public class ErrorResult
{

	private String code = null;
	private String message = null;
	private List<String> trace = new ArrayList<>();


	/**
	 * A value from an application enumeration associated with a known error condition, if the nature of the error is
	 * well known.
	 **/
	public ErrorResult code(String code)
	{
		this.code = code;
		return this;
	}


	@ApiModelProperty(
		value = "A value from an application enumeration associated with a known  error condition, if the nature of the error is well known.")
	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}


	public void setCode(String code)
	{
		this.code = code;
	}


	/**
	 * A human readable message describing the error
	 **/
	public ErrorResult message(String message)
	{
		this.message = message;
		return this;
	}


	@ApiModelProperty(required = true, value = "A human readable message describing the error")
	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}


	public void setMessage(String message)
	{
		this.message = message;
	}


	/**
	 * An optional stack trace, if available
	 **/
	public ErrorResult trace(List<String> trace)
	{
		this.trace = trace;
		return this;
	}


	@ApiModelProperty(value = "An optional stack trace, if available")
	@JsonProperty("trace")
	public List<String> getTrace()
	{
		return trace;
	}


	public void setTrace(List<String> trace)
	{
		this.trace = trace;
	}


	@Override
	public boolean equals(Object o)
	{
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }
		ErrorResult errorResult = (ErrorResult) o;
		return Objects.equals(code, errorResult.code) &&
			Objects.equals(message, errorResult.message) && Objects.equals(trace, errorResult.trace);
	}


	@Override
	public int hashCode()
	{
		return Objects.hash(code, message, trace);
	}


	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorResult {\n");

		sb.append("    code: ")
			.append(toIndentedString(code))
			.append("\n");
		sb.append("    message: ")
			.append(toIndentedString(message))
			.append("\n");
		sb.append("    trace: ")
			.append(toIndentedString(trace))
			.append("\n");
		sb.append("}");
		return sb.toString();
	}


	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the first line).
	 */
	private String toIndentedString(Object o)
	{
		if (o == null) { return "null"; }
		return o.toString()
			.replace("\n", "\n    ");
	}
}
