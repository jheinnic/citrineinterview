package info.jchein.citrine.interview.model;

import java.util.Objects;

import javax.measure.Unit;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * Input message requesting conversion of an input quantity, given its  value and units, to an equivalent value in compatible Base SI units.
 **/

@ApiModel(description = "Input message requesting conversion of an input quantity, given its  value and units, to an equivalent value in compatible Base SI units.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-08-17T13:59:12.955-07:00")
public class QuantityToConvert   {
  
  private Double input = null;
	private Unit<?> units = null;

  /**
   * Double precision floating point value defining the quantity to be converted to system units.
   **/
  public QuantityToConvert input(Double input) {
    this.input = input;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Double precision floating point value defining the quantity to be converted to system units.")
  @JsonProperty("input")
  public Double getInput() {
    return input;
  }
  public void setInput(Double input) {
    this.input = input;
  }

  /**
   * Formula expressing units for the the given input quantity to be converted from.
   **/
	public QuantityToConvert units(Unit<?> units)
	{
    this.units = units;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Formula expressing units for the the given input quantity to be converted from.")
  @JsonProperty("units")
	public Unit<?> getUnits()
	{
    return units;
  }


	public void setUnits(Unit<?> units)
	{
    this.units = units;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    QuantityToConvert quantityToConvert = (QuantityToConvert) o;
    return Objects.equals(input, quantityToConvert.input) &&
        Objects.equals(units, quantityToConvert.units);
  }

  @Override
  public int hashCode() {
    return Objects.hash(input, units);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuantityToConvert {\n");
    
    sb.append("    input: ").append(toIndentedString(input)).append("\n");
    sb.append("    units: ").append(toIndentedString(units)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

