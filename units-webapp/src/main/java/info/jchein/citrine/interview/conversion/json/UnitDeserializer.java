package info.jchein.citrine.interview.conversion.json;


import java.io.IOException;

import javax.measure.Unit;
import javax.measure.format.UnitFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;


public class UnitDeserializer
extends JsonDeserializer<Unit<?>>
{
	private static final Logger LOG = LoggerFactory.getLogger(UnitDeserializer.class);

	// This must be injected as a scoped proxy to enable multi-threaded use where the scope
	// identity either is itself a thread or something object that is bound to a single thread.
	private final UnitFormat unitFormat;


	public UnitDeserializer( UnitFormat unitFormat )
	{
		this.unitFormat = unitFormat;
	}


	@Override
	public Unit<?> deserialize(JsonParser jp, DeserializationContext ctxt)
	throws IOException, JsonProcessingException
	{
		String unitFormula = jp.getCodec().readValue(jp, String.class);
		try {
			return unitFormat.parse(unitFormula);
		}
		catch (Exception exp) {
			LOG.error("Could not interpret input type formula, {}", unitFormula);
			throw new IllegalArgumentException("Could not interpret input type formula", exp);
		}
	}
}
