package info.jchein.citrine.interview.api;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-17T14:03:32.044-07:00")

public class NotFoundException extends ApiException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1169236164369079100L;

	public NotFoundException (int code, String msg) {
		super(code, msg);
	}
}
