package info.jchein.citrine.interview.api;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-17T14:03:32.044-07:00")

public class ApiException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 80757901242615404L;
	private int code;
	public ApiException (int code, String msg) {
		super(msg);
		this.code = code;
	}


	public int getCode()
	{
		return this.code;
	}
}
