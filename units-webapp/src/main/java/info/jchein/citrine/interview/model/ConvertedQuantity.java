package info.jchein.citrine.interview.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * Output from a quantity conversion request, consisting of a double precision floating point value, a compound unit expression made of base SI units, and a conversion ratio from input to output quantities resulting from the change of units.
 **/

@ApiModel(description = "Output from a quantity conversion request, consisting of a double precision floating point value, a compound unit expression made of base SI units, and a conversion ratio from input to output quantities resulting from the change of units.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-08-17T13:59:12.955-07:00")
public class ConvertedQuantity   {
  
  private Double multiplicationFactor = null;
  private Double output = null;
  private String unitName = null;

  /**
   * Conversion ratio to returned base SI unit from query quantity used  to retrieve it.
   * minimum: 0.0
   **/
  public ConvertedQuantity multiplicationFactor(Double multiplicationFactor) {
    this.multiplicationFactor = multiplicationFactor;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Conversion ratio to returned base SI unit from query quantity used  to retrieve it.")
  @JsonProperty("multiplication_factor")
  public Double getMultiplicationFactor() {
    return multiplicationFactor;
  }
  public void setMultiplicationFactor(Double multiplicationFactor) {
    this.multiplicationFactor = multiplicationFactor;
  }

  /**
   * Output value after converting query quantity to base SI units.
   **/
  public ConvertedQuantity output(Double output) {
    this.output = output;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Output value after converting query quantity to base SI units.")
  @JsonProperty("output")
  public Double getOutput() {
    return output;
  }
  public void setOutput(Double output) {
    this.output = output;
  }

  /**
   * Unit of equivalent dimension, expressed as a formula for base SI units
   **/
  public ConvertedQuantity unitName(String unitName) {
    this.unitName = unitName;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Unit of equivalent dimension, expressed as a formula for base SI units")
  @JsonProperty("unit_name")
  public String getUnitName() {
    return unitName;
  }
  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConvertedQuantity convertedQuantity = (ConvertedQuantity) o;
    return Objects.equals(multiplicationFactor, convertedQuantity.multiplicationFactor) &&
        Objects.equals(output, convertedQuantity.output) &&
        Objects.equals(unitName, convertedQuantity.unitName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(multiplicationFactor, output, unitName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConvertedQuantity {\n");
    
    sb.append("    multiplicationFactor: ").append(toIndentedString(multiplicationFactor)).append("\n");
    sb.append("    output: ").append(toIndentedString(output)).append("\n");
    sb.append("    unitName: ").append(toIndentedString(unitName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

