package info.jchein.citrine.interview.application;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.ScopedProxyMode;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableAutoConfiguration
@EnableSwagger2
@PropertySource("classpath:application.properties")
@ComponentScan(
	basePackages = "info.jchein.citrine.interview", 
	scopedProxy = ScopedProxyMode.INTERFACES,
	lazyInit = false)
public class BootUnitConversionApp
implements CommandLineRunner
{
	static Logger LOGGER = LoggerFactory.getLogger(BootUnitConversionApp.class);


	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(BootUnitConversionApp.class, args);
	}


	@Override
	public void run(String... arg0) throws Exception
	{
		if (arg0.length > 0 && arg0[0].equals("exitcode")) { throw new ExitException(); }
	}


	@Bean
	protected ServletContextListener listener()
	{
		return new ServletContextListener() {
			@Override
			public void contextInitialized(ServletContextEvent sce)
			{
				LOGGER.info("ServletContext initialized");
			}


			@Override
			public void contextDestroyed(ServletContextEvent sce)
			{
				LOGGER.info("ServletContext destroyed");
			}
		};
	}


	class ExitException
	extends RuntimeException
	implements ExitCodeGenerator
	{
		private static final long serialVersionUID = 1L;


		@Override
		public int getExitCode()
		{
			return 10;
		}
	}
}
