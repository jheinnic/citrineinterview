package info.jchein.citrine.interview.configuration;


import javax.measure.Unit;
import javax.measure.format.UnitFormat;
import javax.measure.spi.UnitFormatService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import info.jchein.citrine.interview.conversion.UnitConverter;
import info.jchein.citrine.interview.conversion.json.UnitDeserializer;
import info.jchein.citrine.interview.conversion.json.UnitSerializer;


@Configuration
public class UnitsUIConfiguration
extends WebMvcConfigurerAdapter
{
	// private final Logger LOG = LoggerFactory.getLogger(UnitsUIConfiguration.class);

	// @Autowired
	// UnitFormatService unitFormatService;

	@Autowired
	UnitConverter unitConverter;

	@Bean
	@Qualifier("Citrine")
	@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.INTERFACES)
	public UnitFormat unitFormat(UnitFormatService unitFormatService)
	{
		return unitFormatService.getUnitFormat("Citrine");
	}
	
	// @Bean
	// public UnitConverter unitConverter()
	// {
	// final UnitConverter retVal = new UnitConverter();
	// retVal.setUnitFormat(unitFormat());
	// return retVal;
	// }
	//
	//
	// @Bean
	// public ConversionServiceFactoryBean conversionServiceFactoryBean()
	// {
	// LOG.warn("Creating conversion service factory bean!");
	// ConversionServiceFactoryBean retVal = new ConversionServiceFactoryBean();
	// retVal.setConverters(Collections.singleton(unitConverter()));
	// return retVal;
	// }


	@Override
	public void addFormatters(final FormatterRegistry registry)
	{
		registry.addConverter(String.class, Unit.class, unitConverter);
	}


	@Bean
	public UnitsJacksonCustomizer jacksonCustomizer(UnitFormat unitFormat)
	{
		return new UnitsJacksonCustomizer(unitFormat);
	}


	private static final class UnitsJacksonCustomizer
	implements Jackson2ObjectMapperBuilderCustomizer
	{
		private final UnitFormat unitFormat;


		UnitsJacksonCustomizer( UnitFormat unitFormat )
		{
			this.unitFormat = unitFormat;
		}


		@Override
		public void customize(Jackson2ObjectMapperBuilder builder)
		{
			final UnitDeserializer unitDeserializer = new UnitDeserializer(unitFormat);
			builder.deserializerByType(Unit.class, unitDeserializer);

			final UnitSerializer unitSerializer = new UnitSerializer(unitFormat);
			builder.serializerByType(Unit.class, unitSerializer);
		}
	}
}
