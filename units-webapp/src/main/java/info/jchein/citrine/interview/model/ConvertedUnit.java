package info.jchein.citrine.interview.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



/**
 * Output from a units conversion request, consisting of a compound unit expression made of base SI units and the conversion ratio from for hypothetical quantities when mapped from given input units to this messages output units.
 **/

@ApiModel(description = "Output from a units conversion request, consisting of a compound unit expression made of base SI units and the conversion ratio from for hypothetical quantities when mapped from given input units to this messages output units.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2016-08-17T13:59:12.955-07:00")
public class ConvertedUnit   {
  
  private Double multiplicationFactor = null;
  private String unitName = null;

  /**
   * Conversion ratio to returned base SI unit from the query unit used  to retrieve it.
   * minimum: 0.0
   **/
  public ConvertedUnit multiplicationFactor(Double multiplicationFactor) {
    this.multiplicationFactor = multiplicationFactor;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Conversion ratio to returned base SI unit from the query unit used  to retrieve it.")
  @JsonProperty("multiplication_factor")
  public Double getMultiplicationFactor() {
    return multiplicationFactor;
  }
  public void setMultiplicationFactor(Double multiplicationFactor) {
    this.multiplicationFactor = multiplicationFactor;
  }

  /**
   * Unit of equivalent dimension, expressed as a formula for base SI units
   **/
  public ConvertedUnit unitName(String unitName) {
    this.unitName = unitName;
    return this;
  }

  
  @ApiModelProperty(required = true, value = "Unit of equivalent dimension, expressed as a formula for base SI units")
  @JsonProperty("unit_name")
  public String getUnitName() {
    return unitName;
  }
  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConvertedUnit convertedUnits = (ConvertedUnit) o;
    return Objects.equals(multiplicationFactor, convertedUnits.multiplicationFactor) &&
        Objects.equals(unitName, convertedUnits.unitName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(multiplicationFactor, unitName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConvertedUnits {\n");
    
    sb.append("    multiplicationFactor: ").append(toIndentedString(multiplicationFactor)).append("\n");
    sb.append("    unitName: ").append(toIndentedString(unitName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

