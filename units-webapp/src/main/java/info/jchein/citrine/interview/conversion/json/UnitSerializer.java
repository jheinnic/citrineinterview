package info.jchein.citrine.interview.conversion.json;


import java.io.IOException;

import javax.measure.Unit;
import javax.measure.format.UnitFormat;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


public class UnitSerializer
extends JsonSerializer<Unit<?>>
{
	// private static final Logger LOG = LoggerFactory.getLogger(UnitSerializer.class);

	// This must be injected as a scoped proxy to enable multi-threaded use where the scope
	// identity either is itself a thread or something object that is bound to a single thread.

	// TODO: Make sure this works for formatting before weaning off UnitImpl.toString()
	@SuppressWarnings("unused")
	private final UnitFormat unitFormat;


	public UnitSerializer( @NotNull @Autowired UnitFormat unitFormat )
	{
		this.unitFormat = unitFormat;
	}



	@Override
	public void serialize(@NotNull Unit<?> value, @NotNull JsonGenerator gen,
		@NotNull SerializerProvider serializers)
	throws IOException, JsonProcessingException
	{
		final String unitFormula = value.toString();
		gen.writeString(unitFormula);
	}
}
