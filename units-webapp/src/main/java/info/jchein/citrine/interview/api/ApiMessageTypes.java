package info.jchein.citrine.interview.api;


import java.util.HashMap;

import org.springframework.http.MediaType;


public enum ApiMessageTypes
{
	CONVERTED_QUANTITY("ConvertedQuantity", "1.0.0"),
	CONVERTED_UNIT("ConvertedUnit", "1.0.0"),
	QUANTITY_TO_CONVERT("QuantityToConvert", "1.0.0");

	private MediaType mediaType;

	public static final String SCHEMA_PARAMETER_NAME = "schema";
	public static final String MESSAGE_PARAMETER_NAME = "message";
	public static final String REVISION_PARAMETER_NAME = "revision";
	public static final String SCHEMA_VALUE_NAME = "citrineUnits.json";

	public static final String CONVERTED_QUANTITY_VALUE =
		MediaType.APPLICATION_JSON_UTF8_VALUE +
			";schema=citrineUnits.json;message=ConvertedQuantity;revision=1.0.0";
	public static final String CONVERTED_UNIT_VALUE =
		MediaType.APPLICATION_JSON_UTF8_VALUE +
			";schema=citrineUnits.json;message=ConvertedUnit;revision=1.0.0";
	public static final String QUANTITY_TO_CONVERT_VALUE =
		MediaType.APPLICATION_JSON_UTF8_VALUE +
			";schema=citrineUnits.json;message=QuantityToConvert;revision=1.0.0";


	private ApiMessageTypes( String messageName, String revision )
	{
		this.mediaType = createMediaType(messageName, revision);
	}


	private static final MediaType createMediaType(String messageName, String revision)
	{
		final HashMap<String, String> mediaParams = new HashMap<>();
		mediaParams.put(SCHEMA_PARAMETER_NAME, SCHEMA_VALUE_NAME);
		mediaParams.put(MESSAGE_PARAMETER_NAME, messageName);
		mediaParams.put(REVISION_PARAMETER_NAME, revision);

		return new MediaType(MediaType.APPLICATION_JSON_UTF8, mediaParams);
	}


	public MediaType getMediaType()
	{
		return this.mediaType;
	}
}
