package info.jchein.citrine.interview.uom.format;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Priority;
import javax.measure.format.UnitFormat;
import javax.measure.spi.UnitFormatService;

import tec.uom.lib.common.function.IntPrioritySupplier;
import tec.uom.se.format.EBNFUnitFormat;
import tec.uom.se.format.LocalUnitFormat;
import tec.uom.se.format.SimpleUnitFormat;
import tec.uom.se.format.SimpleUnitFormat.Flavor;


@Priority(CitrineUnitFormatService.PRIO)
public class CitrineUnitFormatService
implements UnitFormatService, IntPrioritySupplier
{
	static final int PRIO = 10000;

	private final static String DEFAULT_FORMAT = Flavor.Default.name();

	private final Map<String, UnitFormat> formats = new HashMap<>();

	public CitrineUnitFormatService()
	{
		formats.put(DEFAULT_FORMAT, SimpleUnitFormat.getInstance());
		formats.put(Flavor.ASCII.name(), SimpleUnitFormat.getInstance(Flavor.ASCII));
		formats.put("EBNF", EBNFUnitFormat.getInstance());
		formats.put("Local", LocalUnitFormat.getInstance());
		formats.put("Citrine", CitrineUnitFormat.getInstance());
	}


	@Override
	public UnitFormat getUnitFormat(String formatName)
	{
		Objects.requireNonNull(formatName, "Format name required");
		return formats.get(formatName);
	}


	@Override
	public UnitFormat getUnitFormat()
	{
		return formats.get("Citrine");
	}


	@Override
	public Set<String> getAvailableFormatNames()
	{
		return new HashSet<>(formats.keySet());
	}


	@Override
	public int getPriority()
	{
		return PRIO;
	}
}
