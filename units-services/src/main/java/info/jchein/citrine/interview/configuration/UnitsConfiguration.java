package info.jchein.citrine.interview.configuration;


import javax.measure.spi.ServiceProvider;
import javax.measure.spi.SystemOfUnitsService;
import javax.measure.spi.UnitFormatService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class UnitsConfiguration
{
	@Bean
	public ServiceProvider getServiceProvider()
	{
		return ServiceProvider.current();
	}


	@Bean
	public SystemOfUnitsService getSystemOfUnitsService()
	{
		final ServiceProvider provider = getServiceProvider();
		return provider.getSystemOfUnitsService();
	}


	@Bean
	public UnitFormatService getUnitFormatService()
	{
		final ServiceProvider provider = getServiceProvider();
		return provider.getUnitFormatService();
	}
}
